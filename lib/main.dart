import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'views/MainPage.dart';
import 'game/board/numQuiz.dart';

void main() {
  runApp(ChangeNotifierProvider(
    create: (context) => NumQuizCounting(),
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Maths For Kids',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: MainPage(),
      ),
    );
  }
}
