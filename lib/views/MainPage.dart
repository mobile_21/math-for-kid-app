import 'package:flutter/material.dart';

import '../game/Questions.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/bg-white.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 300.0),
            child: Column(
              children: [
                const Spacer(
                  flex: 3,
                ),
                Image.asset("assets/mathLogo-removebg.png"),
                const Spacer(
                  flex: 5,
                ),
                OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      fixedSize: const Size(300, 100),
                      backgroundColor: Colors.cyan[900],
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return ScreenGame();
                      }));
                    },
                    child: const Text(
                      'Start Game!',
                      style: TextStyle(fontSize: 35, color: Colors.white),
                    )),
                const Spacer(
                  flex: 5,
                ),
              ],
            ),
          ),
        ),
      ),
    );
    
  }
}
