import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:math_for_kids/views/MainPage.dart';

class dialogQuiz extends StatelessWidget {
  const dialogQuiz({Key? key}) : super(key: key);

  void _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Column(
            children: [
              Text(
                'Game Finish!',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 8),
              Text(
                'Your score : ',
                style: TextStyle(
                  fontSize: 24,
                  color: Colors.grey,
                ),
              ),
            ],
          ),
          content: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                '🎉🎉🎉',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          actions: [
            CupertinoDialogAction(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MainPage()),
                );},
              child: Text('Back to home',
                style: TextStyle(
                  fontSize: 20,
                ),),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepPurple[200],
      body: Center(
        child: MaterialButton(
          color: Colors.deepPurple[100],
          onPressed: () {
            _showDialog(context);
          },
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text('SHOW DIALOG',style: TextStyle(fontSize: 30),),
          ),
        ),
      ),
    );
  }
}
