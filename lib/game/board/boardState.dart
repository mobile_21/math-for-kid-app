import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'numQuiz.dart';

class boardGame extends StatelessWidget {
  const boardGame({super.key});

  @override
  Widget build(BuildContext context) {
    final NumQuizCounting numQuiz = Provider.of<NumQuizCounting>(context);
    return
      Text(
        "NO. ${numQuiz.number}",
        style: const TextStyle(
          fontSize: 46.0,
          color: Colors.black,
          fontFamily: "Satisfy",
        ),
      );
  }
}